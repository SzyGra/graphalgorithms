import numpy as np
import re

#zadanie 2a
#macierz przyleglosci
matrixPrzyleglosci = []

f = open("D:\\PythonoweSkrypty\PythonApplication3\PythonApplication3\\matrixDFS.txt", 'r')


for line in f:
    numbers = re.findall(r'[0-1]',line)
    matrixPrzyleglosci.append(numbers)

#Slownik nastepnikow
nastepnik = dict()

i = 0

for x in matrixPrzyleglosci:
    j = 0
    wierzcholek = []
    for number in x:
        number = int(number)
        if(number ==1):
            wierzcholek.append(j)
        j=j+1

    nastepnik[i] = wierzcholek
    i = i+1

print('Lista nastepnikow: ')
print(nastepnik)
print('\n')
#DFS
SkladoweSpojnosci = []
stos = []
rozpatrz=[]
odwiedzone=[]
v = int(input("Podaj indeks wierzcholka poczatkowego: "))
if(v not in nastepnik):
	print("Nie ma takiego wierzcholka w grafie")
	exit()
rozpatrz.append(v)
stos.append(v)
while(1):
        tab_rozp=[]
        zakoncz=True
        tab_rozp.append(v)
        while(1):
                if(not stos):
                        break
                if(nastepnik[v]):
                        powrot=True
                        for x in nastepnik[v]:
                                if(x not in rozpatrz):
                                        v=x
                                        rozpatrz.append(v)
                                        stos.append(v)
                                        tab_rozp.append(v)
                                        powrot=False
                                        break
                        if powrot:
                                stos.pop()
                                if (not stos):
                                        break           
                                v = stos[-1]
                        if(not nastepnik[v]):
                                stos.pop()
                                if (not stos):
                                        break
                                v = stos[-1]
        SkladoweSpojnosci.append(list(tab_rozp))
        for i in range(len(nastepnik)-1):
                if (i+1) not in rozpatrz:
                    zakoncz = False    
                    v = i+1
                    stos.append(v)
                    rozpatrz.append(v)
                    break
        if(zakoncz):
                break

print("Wierzcholki w kolejnosci ich rozpatrywania: ")
print(rozpatrz)
print("Liczba skladowych spojnosci: {0}".format(len(SkladoweSpojnosci)))
c=1
print('Kolejne skladowe: ')
for skladowa in SkladoweSpojnosci:
    print("{0} : {1}".format(c, skladowa))
    c=c+1