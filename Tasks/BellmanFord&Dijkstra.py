# GRAD SZYMON
# CW.3
# Python
import re
import math

macierzWag = []

f = open("D:\\PythonoweSkrypty\PythonApplication3\PythonApplication3\\MatrixPaths.txt", 'r')
#Waruenk dla Bellmana-Forda
belman_match=re.compile(r'^-[0-9]$')
zmien=True
#Wybor algorytmu
choice=''
minus_wages=0
u=1

graf={}
thing=''
for line in f:
    numbers = re.findall(r'-?[0-9]?',line.strip())
    while thing in numbers: numbers.remove(thing)  
    macierzWag.append(numbers)
for wage,mage in zip(macierzWag,range(1,len(macierzWag)+1)):
        pomocnicza_cyfra=1
        table={}
        for one_wage in wage:
                if(one_wage!='-'):
                        table[pomocnicza_cyfra]=one_wage
                        graf[mage]=table
                pomocnicza_cyfra=pomocnicza_cyfra+1
                if belman_match.match(one_wage):
                        minus_wages=minus_wages+1
# {wierzchołek1: {sąsiad1: waga, sąsiad2: waga...}}
print(graf)
notVisited = list(range(len(graf)))
distances={}

distances[1]=(0,0)
for i in range(2, len(graf) + 1):
	distances[i] = (1000000000000, 0)
#Sprawdzenie, czy przy przejściu daną krawędzią grafu (u,v) z ‘u’ do ‘v’ , nie otrzymamy krótszej niż dotychczasowa ścieżki z ‘s’ do ‘v’. Jeżeli tak, to zmniejszamy oszacowanie wagi najkrótszej ścieżki d[v]
#Wzorowałem się na kodzie ze strony https://gist.github.com/econchick/4666413 i na samym dole jest relaksacja
def relaksacja(waga,source,v):
        newDistance = int(waga)+int(distances[source][0])
        if distances[v][0]>newDistance:
                distances[v]=(newDistance,source)
        return

def BellmanFord(v):
        
        for i in range(len(graf)-1):
                for u in graf:
                        for w,waga in graf[u].items():
                                relaksacja(waga,u,w)
        for u in graf:
                for w,waga in graf[u].items():
                        if(int(distances[w][0])>int(distances[u][0])+int(waga)):
                                print('Jest ujemny cykl. Nie ma rozwiazania')
                                return False
        return True
#Algorytm Dijkstry
def Dijkstra(v):
        notVisited.remove(v)

        for d,waga in graf[v].items():
                relaksacja(waga,v,d)

        if notVisited:
                #posortowanie listy
                posortowane=sorted(distances.items(), key=lambda w: w[1][0])
                for a in posortowane:
                        if a[0] in notVisited:
                                Dijkstra(a[0])
        return
#Wyswietlanie najkrotszej sciezki
def pathPrint():
        for i in graf.keys():
                obecnie=i
                S=[]
                S.append(obecnie)
                print('{0}'.format(obecnie),end=': ')
                while obecnie != 1:
                        obecnie=distances[obecnie][1]
                        S.append(obecnie)

                SPrint=list(reversed(S))
                for v in SPrint:
                        print(v,end=' ')
                print('ma dlugosc {0}'.format(distances[i][0]))



if minus_wages>0:
        choice='bellman'

if choice=='bellman':
        print('Są wagi ujemne. Korzystam z algorytmu Bellmana-Forda')
        print('Najkrotsza sciezka z {0} do:'.format(u))
        
        sprawdz=BellmanFord(u)
        if(sprawdz):
                pathPrint()
else:
        print('Wagi sa dodatnie. Korzystam z algorytmu Dijkstry.')
        print('Najkrotsza sciezka z {0} do:'.format(u))
        Dijkstra(u)
        pathPrint()

        
        


